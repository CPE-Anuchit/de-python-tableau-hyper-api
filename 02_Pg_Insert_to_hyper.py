import psycopg2
from tableauhyperapi import HyperProcess, Connection, Telemetry, CreateMode, Inserter, TableName

hostname = "host_db"
username = "db_username"
password = "db_password"
database = "db_name"

# เชื่อมต่อฐานข้อมูล postgres
conn = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
cur = conn.cursor()

# Select data from database
cur.execute("SELECT * FROM public.employee_ex1")

# fetchchall 
results = cur.fetchall()

with HyperProcess(Telemetry.SEND_USAGE_DATA_TO_TABLEAU) as hyper:
    with Connection(hyper.endpoint, 'hyper/employee_ex1.hyper', CreateMode.NONE) as connection:        

        # Insert two new rows
        with Inserter(connection, TableName('Extract','employee_ex1')) as inserter:
            for row in results:                
                inserter.add_row([row[0], row[1], row[2], row[3], row[4], row[5], row[6]])            
            inserter.execute()

print(f"Insert 'employee_ex1' file hyper successfully.")
conn.close()