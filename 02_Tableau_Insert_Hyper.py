# เรียกใช้งาน Tableau Hyper API
from tableauhyperapi import HyperProcess, \
    Telemetry, \
    Connection, \
    CreateMode, \
    SqlType, \
    TableDefinition, \
    NOT_NULLABLE, \
    escape_string_literal

# folder path
data_path = "data/"
hyper_path = "hyper/"

csv_name = "employee.csv"

# กำหนดชื่อไฟล์ (set filename)
hyper_name = "employee"

# กำหนดชื่อตาราง
table_name = "Employees"

path_data_csv = f"{data_path}/{csv_name}"

# HyperProcess เปิดใช้งาน server
# Telemetry ตั้งค่าการส่งข้อมูลการใช้งานไปยัง Hyper API
#   -Telemetry.SEND_USAGE_DATA_TO_TABLEAU ส่งข้อมูลการใช้งานไปยัง Tableau
#   - Telemetry.DO_NOT_SEND_USAGE_DATA_TO_TABLEAU ยกเลิกการส่งข้อมูล
with HyperProcess(telemetry=Telemetry.SEND_USAGE_DATA_TO_TABLEAU) as hyper:    
   # Connection เปิดการเชื่อมต่อ .hyper file ให้เราสามารถเชื่อมต่อกับไฟล์ hyper.endpoint และการระบุโหมดการเขียน CreateMode.CREATE_AND_REPLACE (เขียนทับ) / CreateMode.None
    with Connection(endpoint=hyper.endpoint, database="hyper/employee_ex1.hyper", create_mode=CreateMode.CREATE_AND_REPLACE) as connection:
        # กำหนด schema และสร้างตาราง
        connection.catalog.create_schema('Extract')          
        customer_table = TableDefinition(
        table_name="employee",
        columns=[
            TableDefinition.Column('PRE_NAME', SqlType.varchar(50)),
            TableDefinition.Column('FIST_NAME', SqlType.varchar(100)),
            TableDefinition.Column('LAST_NAME', SqlType.varchar(100)),
            TableDefinition.Column('BIRTHDAY', SqlType.date()),
            TableDefinition.Column('SEX', SqlType.varchar(1)),
            TableDefinition.Column('CREATED_AT', SqlType.timestamp()),
            TableDefinition.Column('UPDATED_AT', SqlType.timestamp()),                      
            ]
        )
        connection.catalog.create_table(customer_table)
        
        # นำข้อมูลจาก csv เข้าตาราง
        insert_table = connection.execute_command(
        command=f"COPY {customer_table.table_name} from {escape_string_literal(path_data_csv)} with (format csv, NULL 'NULL', delimiter ',', header)"
        )
        
print(f"Insert data to table {table_name} hyper successfully.")