# เรียกใช้งาน Tableau Hyper API
from tableauhyperapi import HyperProcess, \
    Telemetry, \
    Connection, \
    CreateMode, \
    SqlType, \
    TableDefinition, \
    TableName

# HyperProcess เปิดใช้งาน server
# Telemetry ตั้งค่าการส่งข้อมูลการใช้งานไปยัง Hyper API
#   -Telemetry.SEND_USAGE_DATA_TO_TABLEAU ส่งข้อมูลการใช้งานไปยัง Tableau
#   - Telemetry.DO_NOT_SEND_USAGE_DATA_TO_TABLEAU ยกเลิกการส่งข้อมูล

with HyperProcess(telemetry=Telemetry.SEND_USAGE_DATA_TO_TABLEAU) as hyper:

    # Connection เปิดการเชื่อมต่อ .hyper file ให้เราสามารถเชื่อมต่อกับไฟล์ hyper.endpoint และการระบุโหมดการเขียน CreateMode.CREATE_AND_REPLACE (เขียนทับ) / CreateMode.None
    with Connection(endpoint=hyper.endpoint, database='hyper/employee_ex1.hyper', create_mode=CreateMode.CREATE_AND_REPLACE) as connection:

        # กำหนด schema 
        connection.catalog.create_schema('Extract')

        # สร้างตาราง กำหนดชื่อ cloumns และ ชนิดของข้อมูล(data type)
        example_table = TableDefinition(TableName('Extract', 'employee_ex1'), [
            TableDefinition.Column('PRE_NAME', SqlType.varchar(50)),
            TableDefinition.Column('FIST_NAME', SqlType.varchar(100)),
            TableDefinition.Column('LAST_NAME', SqlType.varchar(100)),
            TableDefinition.Column('BIRTHDAY', SqlType.date()),
            TableDefinition.Column('SEX', SqlType.varchar(1)),
            TableDefinition.Column('CREATED_AT', SqlType.timestamp()),
            TableDefinition.Column('UPDATED_AT', SqlType.timestamp()),
        ])

        connection.catalog.create_table(example_table)

print(f"Create 'employee' file hyper successfully.")