import tableauserverclient as TSC

server_address = ''
site_name = ''
project_name = ''
token_name = ''
token_value = ''
path_to_database = ''

tableau_auth = TSC.PersonalAccessTokenAuth(token_name=token_name, personal_access_token=token_value, site_id=site_name)
server = TSC.Server(server_address, use_server_version=True)
with server.auth.sign_in(tableau_auth):
    publish_mode = TSC.Server.PublishMode.Overwrite
    all_projects, pagination_item = server.projects.get()
    
    for project in TSC.Pager(server.projects):        
        if project.name == project_name:
            project_id = project.id
            print(project_id)
    
    datasource = TSC.DatasourceItem(project_id)
    datasource = server.datasources.publish(datasource, path_to_database, publish_mode)

print(f"Tableau Hyper Api publish data to server successfully.")