# เรียกใช้งาน Tableau Hyper API
from tableauhyperapi import HyperProcess, \
    Telemetry, \
    Connection, \
    TableName

# HyperProcess เปิดใช้งาน server
# Telemetry ตั้งค่าการส่งข้อมูลการใช้งานไปยัง Hyper API
#   -Telemetry.SEND_USAGE_DATA_TO_TABLEAU ส่งข้อมูลการใช้งานไปยัง Tableau
#   - Telemetry.DO_NOT_SEND_USAGE_DATA_TO_TABLEAU ยกเลิกการส่งข้อมูล
with HyperProcess(telemetry=Telemetry.SEND_USAGE_DATA_TO_TABLEAU) as hyper:
    
    # Connection เปิดการเชื่อมต่อ .hyper file ให้เราสามารถเชื่อมต่อกับไฟล์ hyper.endpoint
    with Connection(endpoint=hyper.endpoint, database='hyper/employee_ex1.hyper') as connection:

        # ดึงข้อมูลออกมากแสดงทั้งหมด
        rows_in_table = connection.execute_query(query=f"SELECT * FROM {TableName('Extract', 'employee_ex1')}")
        rows = list(rows_in_table)
        for row in rows:
            print(row)

